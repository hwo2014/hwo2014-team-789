_utils = {

	carData: undefined,
	carIndex: 0,
	trackData: undefined,
	currentPositions: undefined,
	lastSpeed: 0.0,
	maxSpeed: 0.0,
	lastInPieceDistance: 0.0,

	setCarData: function(carData) {

		_utils.carData = carData;

		console.log("Name: " + carData.name);
		console.log("Color: " + carData.color);

	},


	setTrackData: function(trackData) {

		_utils.trackData = trackData;

		// Log out data
		for (var i = 0; i < trackData.pieces.length; i++) {

			var piece = trackData.pieces[i];

			if (piece.length) {
				console.log("Length: " + piece.length);
			}

			if (piece.radius) {
				console.log("Radius: " + piece.radius);
			}

		}

	},


	setCurrentPositions: function(posData) {


		for (var i = 0; i < posData.length; i++) {
			if (posData[i].id.name == _utils.carData.name) {

				_utils.carIndex = i;

			}
		}

		var changed = false;

		if (_utils.currentPositions) {
			if (_utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex != posData[_utils.carIndex].piecePosition.pieceIndex) {
				changed = true;
			}
		} else {
			changed = true;
		}

		_utils.currentPositions = posData;

		if (changed) {

			_utils.pieceChanged();

		} else {

			var currentInPieceDistance = _utils.currentPositions[_utils.carIndex].piecePosition.inPieceDistance;
			_utils.lastSpeed = currentInPieceDistance - _utils.lastInPieceDistance;

			if (_utils.lastSpeed > _utils.maxSpeed) {

				_utils.maxSpeed = _utils.lastSpeed;

			}


		}

		_utils.lastInPieceDistance = _utils.currentPositions[_utils.carIndex].piecePosition.inPieceDistance;


		//console.log("Speed: " + _utils.lastSpeed + " / " + _utils.maxSpeed);
		//console.log("InPieceDistance: " + _utils.currentPositions[_utils.carIndex].piecePosition.inPieceDistance);


	},

	pieceChanged: function() {

		var cp = _utils.currentPiece();

		if (cp.length) {

			console.log("Length Piece");

		} else if (cp.radius) {

			console.log("Radius Piece");

		}

	},

	currentPiece: function() {

		return _utils.trackData.pieces[_utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex];

	},

	nextPiece: function() {

		var ni = _utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex + 1;
		if (ni == _utils.trackData.pieces.length) {
			ni = 0;
		}

		return _utils.trackData.pieces[ni];

	},

	nextRadius: function() {

		found = false;
		var ni = _utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex + 1;
		if (ni == _utils.trackData.pieces.length) {
			ni = 0;
		}

		do {

			//Intentional check two pieces in advance.
			ni = ni + 1;
			if (ni == _utils.trackData.pieces.length) {
				ni = 0;
			}

			var np = _utils.trackData.pieces[ni];
			if (np.radius) {
				found = true;
				return np;
			}


		} while (!found);


	},

	slipAngle: function() {

		return _utils.currentPositions[_utils.carIndex].angle;

	},

	slipAmount: function() {

		return Math.abs(_utils.currentPositions[_utils.carIndex].angle);

	},

	distanceToNextRadius: function() {

		var cp = _utils.currentPiece();
		//var np = _utils.nextPiece();

		if (cp.radius) {

			return 0.0;

		} else {

			var ni = _utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex
			var distance = cp.length - _utils.lastInPieceDistance;
			var found = false;

			do {

				ni = ni + 1;
				if (ni == _utils.trackData.pieces.length) {
					ni = 0;
				}

				var np = _utils.trackData.pieces[ni];
				if (np.radius) {
					found = true;
				}
				else
				{
					distance = distance + np.length;
				}


			} while (!found);

			return distance;

		}

		/*
		} else if (np.radius) {

			return cp.length - _utils.lastInPieceDistance;

		} else {

			return np.length;

		}
		*/

	},

	radiusPiecesBehind : function() {

		found = false;
		var ni = _utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex;
		var rcCount = 0;

		do {

			ni = ni - 1;
			if (ni < 0) {
				ni = _utils.trackData.pieces.length-1;
			}

			var np = _utils.trackData.pieces[ni];
			if (np.radius) {
				rcCount = rcCount + 1;		
			} else {
				found = true;
				return rcCount;
			}


		} while (!found);

	},


	radiusPiecesAhead : function() {

		found = false;
		var ni = _utils.currentPositions[_utils.carIndex].piecePosition.pieceIndex;
		var rcCount = 0;

		do {

			ni = ni + 1;
			if (ni == _utils.trackData.pieces.length) {
				ni = 0;
			}

			var np = _utils.trackData.pieces[ni];
			if (np.radius) {
				rcCount = rcCount + 1;		
			} else {
				found = true;
				return rcCount;
			}


		} while (!found);

	},


	slowZone: 175.0,
	idealNextSpeed: function() {

		var dnr = _utils.distanceToNextRadius();

		if (dnr > _utils.slowZone) {

			// Open straight, go crazy
			console.log("Ideal is max: 10.0");

			return 10.0;

		} else if (dnr > 0.0) {

			// Approaching Bend
			var cp = _utils.currentPiece();
			var rp = _utils.nextPiece();
			var ifr = _utils.idealForRadius(rp.angle);
			var dif = 10.0 - ifr;

			var nextDnr = dnr - _utils.lastSpeed;
			if (nextDnr < 0.0) nextDnr = 0.001;

			var ideal = ifr + (dif * nextDnr / _utils.slowZone);

			console.log("Next ideal is: " + ideal + "    | due to DNR: " + dnr);

			return ideal;

		} else {

			// Bend position
			var rpb = _utils.radiusPiecesBehind();
			var rpa = _utils.radiusPiecesAhead();
			var max = 10.0;
			if (rpb < rpa) {

				console.log("ENTERING BEND");

				max = 8.0;

			} else {

				console.log("EXITING BEND");

			}


			// Just base this on the slide amount
			var sa = _utils.slipAmount();

			// NOTE: this is very close to borderline
			if (sa > 90.0) sa = 90;
			var ideal = max - (sa / 9.0);
			if (ideal < 0.0) ideal = 0.0;

			// A little more balance against straights, 
			// Above is more idea, but maybe we can build in some clever checks
			// on S curves...
			//if (sa > 80.0) sa = 80;
			//var ideal = max - (sa / 8.0);
			//if (ideal < 0.0) ideal = 0.0;

			console.log("Slippery Ideal: " + ideal + "    | due to amount" + sa);

			return ideal;


		}

	},

	idealForRadius: function(angle) {

		return 5.0;
		//return (90.0 - angle) / 10.0;

	},


	shouldSwitch: function() {

		var np = _utils.nextPiece();
		if (np.
			switch) {

			var idealLane = 0;

			var nr = _utils.nextRadius();
			if (nr.angle > 0.0) {

				idealLane = _utils.trackData.lanes.length - 1;

			}

			var carLane = _utils.currentPositions[_utils.carIndex].piecePosition.lane.startLaneIndex;

			console.log("Ideal Lane: " + idealLane + "  | Car Lane: " + carLane);

			if (carLane < idealLane) {

				console.log("Switch Right");

				return "Right";



			} else if (carLane > idealLane) {

				console.log("Switch Left");

				return "Left";

			}


		}

		return false;

	},



}