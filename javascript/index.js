require('./utils');

var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];



console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};


function simpleUpDown() {

  if (_utils.currentPiece().length) {

    console.log("Throttle up");
    send({
      msgType: "throttle",
      data: 1.0
    });

  } else if (_utils.currentPiece().radius) {

    console.log("Throttle down");
    send({
      msgType: "throttle",
      data: 0.3
    });

  }

}

function fudgeUpDown() {

  var dnr = _utils.distanceToNextRadius();

  if (dnr == 0.0) {

    console.log("Throttle medium: " + _utils.lastSpeed);
    send({
      msgType: "throttle",
      data: 0.2
    });

  } else if (dnr < 10.0) {

    if (_utils.lastSpeed > 5.0) {

      console.log("Brake Brake Brake: " + _utils.lastSpeed);
      send({
        msgType: "throttle",
        data: 0.0
      });

    } else {

      console.log("Throttle medium: " + _utils.lastSpeed);
      send({
        msgType: "throttle",
        data: 0.5
      });

    }



  } else if (dnr < 30.0) {

    var throttle = ((dnr - 10.0) / 20.0);

    console.log("Throttle down: [ " + throttle + " ] " + _utils.lastSpeed);
    send({
      msgType: "throttle",
      data: throttle
    });

  } else {

    console.log("Throttle up: " + _utils.lastSpeed);
    send({
      msgType: "throttle",
      data: 1.0
    });

  }

}

var switching = false;

function idealUpDown() {

  
  var switchSent = false;

  var sw = _utils.shouldSwitch();
  if (sw) {

    if (!switching) {

      send({
        msgType: "switchLane",
        data: sw
      });
      switchSent = true;
      console.log("Sending switch message: " + sw);

      switching = true;

    }

  } else {

    switching = false;

  }

  if (!switchSent) {

    var ins = _utils.idealNextSpeed();

    if (_utils.lastSpeed > ins) {
      console.log("Throttle down: " + _utils.lastSpeed);
      send({
        msgType: "throttle",
        data: 0.0
      });
    } else {
      console.log("Throttle up: " + _utils.lastSpeed);
      send({
        msgType: "throttle",
        data: 1.0
      });
    }


  }

}


jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'gameInit') {

    _utils.setTrackData(data.data.race.track);

    send({
      msgType: "ping",
      data: {}
    });

  } else if (data.msgType === 'carPositions') {

    _utils.setCurrentPositions(data.data);

    /*
    simpleUpDown();
    fudgeUpDown();
    */

    idealUpDown();



  } else if (data.msgType === 'yourCar') {

    _utils.setCarData(data.data);

    send({
      msgType: "ping",
      data: {}
    });

  } else {

    console.log("Message: " + data.msgType)

    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});